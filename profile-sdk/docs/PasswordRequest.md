
# PasswordRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**password** | **String** |  |  [optional]
**token** | **String** |  |  [optional]



