
# PersonaRequestWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**personas** | [**List&lt;PersonaResponse&gt;**](PersonaResponse.md) |  |  [optional]



