
# EmailResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**statusCode** | **String** |  |  [optional]
**token** | **String** |  |  [optional]



