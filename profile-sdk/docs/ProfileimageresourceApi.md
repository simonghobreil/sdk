# ProfileimageresourceApi

All URIs are relative to *https://localhost:8082/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProfileDefaultImageByEmailUsingGET**](ProfileimageresourceApi.md#getProfileDefaultImageByEmailUsingGET) | **GET** /api/profiles/image/getProfileImages | Save profile image
[**saveProfileImagesUsingPOST**](ProfileimageresourceApi.md#saveProfileImagesUsingPOST) | **POST** /api/profiles/image/saveProfileImages | Save profile image


<a name="getProfileDefaultImageByEmailUsingGET"></a>
# **getProfileDefaultImageByEmailUsingGET**
> ProfileImageResponse getProfileDefaultImageByEmailUsingGET(email)

Save profile image

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfileimageresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfileimageresourceApi apiInstance = new ProfileimageresourceApi();
String email = "email_example"; // String | email
try {
    ProfileImageResponse result = apiInstance.getProfileDefaultImageByEmailUsingGET(email);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileimageresourceApi#getProfileDefaultImageByEmailUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| email |

### Return type

[**ProfileImageResponse**](ProfileImageResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="saveProfileImagesUsingPOST"></a>
# **saveProfileImagesUsingPOST**
> ProfileImageResponse saveProfileImagesUsingPOST(profileImageRequest)

Save profile image

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.ProfileimageresourceApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: BasicAuth
HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
BasicAuth.setUsername("YOUR USERNAME");
BasicAuth.setPassword("YOUR PASSWORD");

ProfileimageresourceApi apiInstance = new ProfileimageresourceApi();
ProfileImageRequest profileImageRequest = new ProfileImageRequest(); // ProfileImageRequest | profileImageRequest
try {
    ProfileImageResponse result = apiInstance.saveProfileImagesUsingPOST(profileImageRequest);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProfileimageresourceApi#saveProfileImagesUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profileImageRequest** | [**ProfileImageRequest**](ProfileImageRequest.md)| profileImageRequest |

### Return type

[**ProfileImageResponse**](ProfileImageResponse.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

