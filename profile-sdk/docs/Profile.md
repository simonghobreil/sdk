
# Profile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**enable** | **Boolean** |  |  [optional]
**fullName** | **String** |  |  [optional]
**gender** | **String** |  |  [optional]
**howDoHearAboutUS** | **String** |  |  [optional]
**id** | **Long** |  |  [optional]
**invited** | **Boolean** |  |  [optional]
**joinDate** | [**DateTime**](DateTime.md) |  |  [optional]
**lastLoginTime** | [**DateTime**](DateTime.md) |  |  [optional]
**lastLogoutTime** | [**DateTime**](DateTime.md) |  |  [optional]
**passwordHash** | **String** |  |  [optional]
**personas** | [**List&lt;Persona&gt;**](Persona.md) |  |  [optional]
**phone** | **String** |  |  [optional]
**role** | [**RoleEnum**](#RoleEnum) |  |  [optional]


<a name="RoleEnum"></a>
## Enum: RoleEnum
Name | Value
---- | -----
USER | &quot;USER&quot;
ADMIN | &quot;ADMIN&quot;



