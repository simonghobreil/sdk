
# StylistPreferences

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**answer** | **String** |  |  [optional]
**id** | **Long** |  |  [optional]
**question** | **String** |  |  [optional]



