
# Persona

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**completed** | **Boolean** |  |  [optional]
**defaultPersona** | **Boolean** |  |  [optional]
**personaGender** | **String** |  |  [optional]
**personaId** | **Long** |  |  [optional]
**personaName** | **String** |  |  [optional]
**personaStatus** | [**PersonaStatusEnum**](#PersonaStatusEnum) |  |  [optional]
**stylistPreferences** | [**List&lt;StylistPreferences&gt;**](StylistPreferences.md) |  |  [optional]


<a name="PersonaStatusEnum"></a>
## Enum: PersonaStatusEnum
Name | Value
---- | -----
INCOMPLETE | &quot;INCOMPLETE&quot;
INCOMPLETE_SKIP | &quot;INCOMPLETE_SKIP&quot;
COMPLETED | &quot;COMPLETED&quot;



