
# ProfileRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profile** | [**Profile**](Profile.md) |  |  [optional]
**stylistPreferences** | [**List&lt;StylistPreferences&gt;**](StylistPreferences.md) |  |  [optional]



