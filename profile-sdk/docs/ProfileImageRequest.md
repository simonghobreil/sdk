
# ProfileImageRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  |  [optional]
**orginalImagePath** | **String** |  |  [optional]
**thumbImagePath** | **String** |  |  [optional]



