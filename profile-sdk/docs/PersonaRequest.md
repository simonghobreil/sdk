
# PersonaRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**completed** | **Boolean** |  |  [optional]
**defaultPersona** | **Boolean** |  |  [optional]
**email** | **String** |  |  [optional]
**id** | **Long** |  |  [optional]
**personaGender** | **String** |  |  [optional]
**personaName** | **String** |  |  [optional]
**personaStatus** | [**PersonaStatusEnum**](#PersonaStatusEnum) |  |  [optional]
**stylistPreferences** | [**List&lt;AbstractStylistPreferences&gt;**](AbstractStylistPreferences.md) |  |  [optional]


<a name="PersonaStatusEnum"></a>
## Enum: PersonaStatusEnum
Name | Value
---- | -----
INCOMPLETE | &quot;INCOMPLETE&quot;
INCOMPLETE_SKIP | &quot;INCOMPLETE_SKIP&quot;
COMPLETED | &quot;COMPLETED&quot;



