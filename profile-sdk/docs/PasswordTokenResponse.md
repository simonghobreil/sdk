
# PasswordTokenResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**profile** | [**Profile**](Profile.md) |  |  [optional]
**statusCode** | **String** |  |  [optional]
**token** | **String** |  |  [optional]



