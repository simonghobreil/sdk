
# ProfileResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  |  [optional]
**statusCode** | **String** |  |  [optional]



