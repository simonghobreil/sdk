
# PhoneResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phone** | **String** |  |  [optional]
**statusCode** | **String** |  |  [optional]



