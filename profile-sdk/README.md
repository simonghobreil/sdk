# swagger-java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.*;
import io.swagger.client.auth.*;
import io.swagger.client.model.*;
import io.swagger.client.api.PersonaresourceApi;

import java.io.File;
import java.util.*;

public class PersonaresourceApiExample {

    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        
        // Configure HTTP basic authorization: BasicAuth
        HttpBasicAuth BasicAuth = (HttpBasicAuth) defaultClient.getAuthentication("BasicAuth");
        BasicAuth.setUsername("YOUR USERNAME");
        BasicAuth.setPassword("YOUR PASSWORD");

        PersonaresourceApi apiInstance = new PersonaresourceApi();
        PersonaRequest request = new PersonaRequest(); // PersonaRequest | request
        try {
            PersonaResponse result = apiInstance.changePersonaToDefaultUsingPOST(request);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling PersonaresourceApi#changePersonaToDefaultUsingPOST");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://localhost:8082/*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*PersonaresourceApi* | [**changePersonaToDefaultUsingPOST**](docs/PersonaresourceApi.md#changePersonaToDefaultUsingPOST) | **POST** /api/persona/changePersonaToDefault | changePersonaToDefault
*PersonaresourceApi* | [**createPersonaWithPreferencesUsingPOST**](docs/PersonaresourceApi.md#createPersonaWithPreferencesUsingPOST) | **POST** /api/persona/createPersonaWithPreferences | createPersonaWithPreferences
*PersonaresourceApi* | [**createPersonaWithoutPreferencesUsingPOST**](docs/PersonaresourceApi.md#createPersonaWithoutPreferencesUsingPOST) | **POST** /api/persona/createPersonaWithoutPreferences | createPersonaWithoutPreferences
*PersonaresourceApi* | [**getAllPersonasUsingGET**](docs/PersonaresourceApi.md#getAllPersonasUsingGET) | **GET** /api/persona/getAllPersonas | getAllPersonas
*PersonaresourceApi* | [**getDefaultPersonaUsingGET**](docs/PersonaresourceApi.md#getDefaultPersonaUsingGET) | **GET** /api/persona/getDefaultPersona | getDefaultPersona
*PersonaresourceApi* | [**updatePersonaStatusUsingPOST**](docs/PersonaresourceApi.md#updatePersonaStatusUsingPOST) | **POST** /api/persona/updateDefaultPersona | updatePersonaStatus
*ProfileimageresourceApi* | [**getProfileDefaultImageByEmailUsingGET**](docs/ProfileimageresourceApi.md#getProfileDefaultImageByEmailUsingGET) | **GET** /api/profiles/image/getProfileImages | Save profile image
*ProfileimageresourceApi* | [**saveProfileImagesUsingPOST**](docs/ProfileimageresourceApi.md#saveProfileImagesUsingPOST) | **POST** /api/profiles/image/saveProfileImages | Save profile image
*ProfilepasswordresetresourceApi* | [**isVaildTokenUsingGET**](docs/ProfilepasswordresetresourceApi.md#isVaildTokenUsingGET) | **GET** /api/token/isVaildPasswordToken | isVaildToken
*ProfilepasswordresetresourceApi* | [**resendVerificationTokenUsingPOST**](docs/ProfilepasswordresetresourceApi.md#resendVerificationTokenUsingPOST) | **POST** /api/token/resendVerificationToken | resendVerificationToken
*ProfilepasswordresetresourceApi* | [**sendResetPasswordTokenUsingGET**](docs/ProfilepasswordresetresourceApi.md#sendResetPasswordTokenUsingGET) | **GET** /api/token/sendResetPasswordToken | Send Reset password token
*ProfilepasswordresetresourceApi* | [**updateVerificationTokenUsingPOST**](docs/ProfilepasswordresetresourceApi.md#updateVerificationTokenUsingPOST) | **POST** /api/token/updateVerificationToken | updateVerificationToken
*ProfileresourceApi* | [**deleteProfileUsingPOST**](docs/ProfileresourceApi.md#deleteProfileUsingPOST) | **POST** /api/profiles/deleteProfile | Delete Profile.
*ProfileresourceApi* | [**getAllProfileBasicInfoUsingGET**](docs/ProfileresourceApi.md#getAllProfileBasicInfoUsingGET) | **GET** /api/profiles/getAllProfileBasicInfo | Get all Users Basic Info By Id
*ProfileresourceApi* | [**getProfileBasicInfoUsingGET**](docs/ProfileresourceApi.md#getProfileBasicInfoUsingGET) | **GET** /api/profiles/getProfileBasicInfo | Get User Basic Info By Id
*ProfileresourceApi* | [**getProfileFullInfoUsingGET**](docs/ProfileresourceApi.md#getProfileFullInfoUsingGET) | **GET** /api/profiles/getUserByEmail | Get Full User Info By Id
*ProfileresourceApi* | [**isVaildProfilePhoneUsingGET**](docs/ProfileresourceApi.md#isVaildProfilePhoneUsingGET) | **GET** /api/profiles/isPhoneExists | Check if phone exists or not
*ProfileresourceApi* | [**isVaildProfileUsingGET**](docs/ProfileresourceApi.md#isVaildProfileUsingGET) | **GET** /api/profiles/isUserExists | Check if user exists or not
*ProfileresourceApi* | [**saveBasicInfoUsingPOST**](docs/ProfileresourceApi.md#saveBasicInfoUsingPOST) | **POST** /api/profiles/saveBasicInfo | Save basic information for profile.
*ProfileresourceApi* | [**saveProfileUsingPOST**](docs/ProfileresourceApi.md#saveProfileUsingPOST) | **POST** /api/profiles/saveProfile | Save Customer profile.
*ProfileresourceApi* | [**updateLastLoginUsingPOST**](docs/ProfileresourceApi.md#updateLastLoginUsingPOST) | **POST** /api/profiles/updateLastLogin | Update last login.
*ProfileresourceApi* | [**updateLastLogoutUsingPOST**](docs/ProfileresourceApi.md#updateLastLogoutUsingPOST) | **POST** /api/profiles/updateLastLogout | Update last logout.
*ProfileresourceApi* | [**updatePasswordUsingPOST**](docs/ProfileresourceApi.md#updatePasswordUsingPOST) | **POST** /api/profiles/updatePassword | Update password.


## Documentation for Models

 - [AbstractStylistPreferences](docs/AbstractStylistPreferences.md)
 - [EmailResponse](docs/EmailResponse.md)
 - [FullProfileResponse](docs/FullProfileResponse.md)
 - [LastSeenRequest](docs/LastSeenRequest.md)
 - [PasswordRequest](docs/PasswordRequest.md)
 - [PasswordTokenResponse](docs/PasswordTokenResponse.md)
 - [Persona](docs/Persona.md)
 - [PersonaRequest](docs/PersonaRequest.md)
 - [PersonaRequestWrapper](docs/PersonaRequestWrapper.md)
 - [PersonaResponse](docs/PersonaResponse.md)
 - [PhoneResponse](docs/PhoneResponse.md)
 - [Profile](docs/Profile.md)
 - [ProfileImageRequest](docs/ProfileImageRequest.md)
 - [ProfileImageResponse](docs/ProfileImageResponse.md)
 - [ProfileRequest](docs/ProfileRequest.md)
 - [ProfileResponse](docs/ProfileResponse.md)
 - [StylistPreferences](docs/StylistPreferences.md)
 - [TokenRequest](docs/TokenRequest.md)
 - [TokenStatusResponse](docs/TokenStatusResponse.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### BasicAuth

- **Type**: HTTP basic authentication


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



